import os
from pymongo import MongoClient

#client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
client = MongoClient("mongodb://database:27017")
db = client.tododb

def add_row(control_dist, open_time, close_time):
    """
    This function takes three values: The distance entered by the user, and the opening and closing
    times of the control point at that distance (which are automatically derived using acp_times.py)
    and inserts them into the database. 
    """
    item = {'distance' : control_dist, 'opening' : open_time, 'closing' : close_time}
    db.datainput.insert_one(item)
    return "OK"

def reset():
    """
    This function simply deletes the current collection. It's very unncessary but makes flask_brevets nicer to read.
    """
    db.datainput.drop()
    return "OK"

def get_data(limit):
    """
    This function will walk through the current items in the 'datainput' collection in the database
    and add each one to a list while stripping it of it's _id value pair. It removes the _id value to make
    displaying the data much simpler. Finally, once all the data is in the list, it returns the list.
    """
    datalist = []                 # create an empty list
    for i in range(limit):        # then loop through 'limit' times from 0 to '1 - limit'
        try:                      # try/except since db.collection.find()[] throws an error when it fails
            datalist.append(db.datainput.find({},{'_id':False}).sort("distance",1)[i])  # append the 'i'th item in datainput collection to the list
        except:
            break                 # if you can't, break the loop
    return datalist
