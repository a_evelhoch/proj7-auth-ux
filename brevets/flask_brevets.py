"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

## TODO: WRITE ALL THE COMMENTS & UPDATE README!!!!!!!!!!!!

import flask
from flask import request, flash, session, redirect, url_for
from flask_restful import Resource, Api
from bson.json_util import dumps
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import mongo_handler # My .py I wrote to handle the mongodb stuff
import password # Copy of .py from proj7-auth-ux
import config
import json
import logging
import os
import time
from flask_login import (LoginManager, current_user, login_required, login_user, logout_user, UserMixin, confirm_login, fresh_login_required)
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from functools import wraps
from wtforms import Form, BooleanField, StringField, PasswordField, validators
from urllib.parse import urlparse, urljoin

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

api = Api(app)

##########################
# Authentication Section #
##########################

##
# Flask-Login Section

login_manager=LoginManager()
login_manager.setup_app(app)
login_manager.login_view = "/login" # When user needs to login, send them to /login
login_manager.login_message = "You must be logged in to view this page!" # Edited the default message

@login_manager.user_loader # load_user function required for Flask-Login
def load_user(id):
    return USERS.get(int(id))

class User(UserMixin):
    """
    This is the user class required for Flask-login. It's pretty basic, but it stores a hashed password too
    """
    def __init__(self, name, id, hashedpass, active=True):
        self.name = name
        self.id = id
        self.hashedPass = hashedpass
        self.active = active

    def is_active(self):
        return self.active

##
# These next two functions are from the slideshows given in class and are for token-based auth

def generate_auth_token(user, expiration):
    """
    Returns an auth token that is hashed with the inputted user's id and expires in 'expiration' seconds
    """
    s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
    return s.dumps({ 'id' : user.id })

def verify_auth_token(token):
    """
    Takes an auth token and checks if it's valid. If it is- returns True, otherwise returns False
    """
    s = Serializer(app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return False
    except BadSignature:
        return False
    return True

##
# A couple globals to make some stuff easier

USERS = {
    0: User(None, 0, None) #It comes with an empty user so that calling len(USERS) will return the number of registered users +1
}

GLOBALTOKEN = "NO_GOOD" # This is the auth token. It has to be stored in python not session so that curl can benefit from it.

##
# These next functions in this section I wrote specifically for this project- they deal with authorization and ui

class addUser(Resource):
    def post(self, nameArg="",passArg=""):
        """
        This is the register user resource: it adds a user with given Username and Password to the global USERS database.
        It can be called as an API using curl's -X POST feature if the data is passed in in the -d flag in this format:
        "username=<name>&password=<password>" and it can also be used as a standalone function with those passed in as args.
        Naturally, the password stored in the global USERS is hashed before being stored.
        """
        if (nameArg == ""):                                     #| This section checks to see if the function was called from python
            inputUsername = str(request.values.get('username')) #| or curl. The nameArg defaults to blank, so if it was called in python
        else:                                                   #| the nameArg will contain the name argument, otherwise it will try
            inputUsername = str(nameArg)                        #| try to get it from request.values.get(), which is where curl's -d
        if (passArg == ""):                                     #| flag passes it in. This section just tries to get the username
            inputPass = str(request.values.get('password'))     #| and password this way and stores them in inputUsername and inputPass
        else:
            inputPass = str(passArg)
        if inputUsername == "" or inputPass == "":  # Make sure the user's not trying to register with blank fields
            return "MISSING_USERNAME_OR_PASSWORD", 401  #If they are, return unauthorized
        for entry in USERS.keys():                  #| Then loop through the list of USERS
            if inputUsername == USERS[entry].name:  #| and look if the username already exists,
                return "USER_ALREADY_EXISTS", 401   #| if it does, return unauth because we can't make that user
        inputPass = password.hash_password(inputPass) # Now that we've verified everything's okay, hash the password
        newUser = User(inputUsername, len(USERS), inputPass) # Then make a new user object with that username and pass.
        userURI = "/api/user/" + str(len(USERS)) # Construct the URI for the new user
        USERS.update({len(USERS): newUser}) # Then add the user to the USERS global list
        return dumps(newUser.__dict__), 201, {'Location': userURI } # Return a JSON of the new users, 201 and the resource location

api.add_resource(addUser, '/api/register') # API location for registering

class userAPIAccess(Resource): # This is the resource to look at a user
    def get(self, id_arg): # It takes an id number as a argument
        data = { "id" : load_user(id_arg).id, "username" : load_user(id_arg).name } # create an object with the users id and name
        return data # and return it

api.add_resource(userAPIAccess, '/api/user/<id_arg>') # API location for looking at users

class getToken(Resource):
    def get(self):
        """
        This is the API resource for generating a token. It's made to be called using curl's -X POST flag.
        A username and password must be supplied using curl's -u flag in this format: <username>:<password>
        It will set the global token valid if the supplied username and password are for a registered user,
        then return that token. If the username and password aren't an existing user and correct, it fails.
        """
        auth = request.authorization # This gets the request from the curl -u field
        if not auth:                            #| If there was no auth, the user didn't provide a username/pass,
            return "MISSING_AUTHORIZATION", 401 #| so return unauthorized
        inputUsername = auth.username # Then get the username and password from the auth
        inputPassword = auth.password
        if inputUsername == "" or inputPassword == "": #| Make sure the username and password are both not blank,
            return "MISSING_USERNAME_OR_PASSWORD", 401 #| if they are, return unauthorized
        userObj = "INVALID" # Create userObj as "INVALID" so we can compare against that if we cant find the user
        for entry in USERS.keys():                 #| Loop through the list of USERS,
            if inputUsername == USERS[entry].name: #| and try to find one with the input username,
                userObj = USERS[entry]             #| if we can, set it.
        if userObj == "INVALID":       #| If userObj is "INVALID" still, we didn't find a user with the input username
            return "NO_SUCH_USER", 401 #| so we return unauthorized
        if (password.verify_password(inputPassword, userObj.hashedPass)): # Once we know its a real user, check the entered pass vs the user's pass
            global GLOBALTOKEN # Make sure we change the GLOBALTOKEN, not the local GLOBALTOKEN
            GLOBALTOKEN = generate_auth_token(userObj, 600) # Then generate a 10-minute token
            return dumps(GLOBALTOKEN) # And return it
        else: # If the entered pass was not the user's pass,
            return "INCORRECT_PASSWORD", 401 # Return unauthorized

api.add_resource(getToken, '/api/token') # API location for getToken

def needTokenOrLogin(func):
    @wraps(func)
    def check_authorization(*args, **kwargs):
        """
        This is a function wrapper that checks all ways the user could be verified before allowing them to see 
        whatever function this is applied to. It's used to allow access of the API to logged-in users, people
        who have gotten an auth token, or people who try to access them with a username and password from curl
        """
        if current_user.is_authenticated: #| If the user was logged-in with Flask-login,
            return func(*args, **kwargs)  #| continue to the function
        auth = request.authorization # Get authorization field from curl, if any
        if verify_auth_token(GLOBALTOKEN):  #| We want to allow users with the auth token in, so check that here
            return func(*args, **kwargs)    #| continue to the function if the token is valid
        elif not auth:                   #| Otherwise, if the auth field wasn't filled out, 
            return "NOT_AUTHORIZED", 401 #| Don't allow access- we don't want non-users to see out APIs
        else: # Otherwise we just repeat the login check functionality from the getToken function for here...
            inputUsername = auth.username
            inputPassword = auth.password
            if inputUsername == "" or inputPassword == "":
                return "NOT_AUTHORIZED", 401
            userObj = "INVALID"
            for entry in USERS.keys():
                if inputUsername == USERS[entry].name:
                    userObj = USERS[entry]
            if userObj == "INVALID":
                return "NOT_AUTHORIZED", 401
            if (password.verify_password(inputPassword, userObj.hashedPass)): #...through here
                return func(*args, **kwargs) # But instead of setting the GLOBALTOKEN, we continue to the function
            return "NOT_AUTHORIZED", 401 # We still return unauthorized if it fails, though
    return check_authorization 

##
# The next three functins are ones I wrote using WTForms to make Flask-login work and provide a nice UX

class loginPageForm(Form):
    """
    This is the WTForms login page form- it has a username text field, password text field and a remember me checkbox
    """
    username = StringField('Username', [validators.Length(min=3, max=25)])
    password = PasswordField('Password', [validators.DataRequired(), validators.Length(min=3, max=25)])
    rememberMe = BooleanField('Remember me')

@app.route('/login', methods=['GET','POST'])
def register():
    """
    This function gets called when any of the submit buttons on the loginPageForm are pressed-
    using the text entered in the username field and the password field it will either attempt to
    register that user into USERS if the Register button is pressed, or log the user in and go to
    the brevets calculator page if the Login button is pressed.
    """
    form = loginPageForm(request.form) # This grabs all the data from the fields in the form
    if request.method == 'POST' and form.validate(): # This if only happens if one of the buttons is pressed
        if request.form['button'] == 'register': # If the button that was pressed is the 'register' button, then...
            thing = addUser.post("self", form.username.data, form.password.data) # attempt to register a user
            if (thing[1] == 201): # If the code returned is 201, then it was successful and we can tell the user
                flash("New user registered") 
            else: # Otherwise the code returned is an error, so...
                flash("Error: " + thing[0]) # tell the user what the error was
        else: # This else is for the pressed button- what that means is this happens if the 'login' button was pressed
            rememberBool = form.rememberMe.data # This is true if the checkbox is checked and false if it isn't
            userObj = "INVALID" # Set a userObj to INVALID so we can check if we got a user later
            for entry in USERS.keys():                      #| Then look through the database of users and,
                if form.username.data == USERS[entry].name: #| If you find a user with the same name,
                    userObj = USERS[entry]                  #| set userObj to it
            if userObj == "INVALID":                #| If no user was found,
                flash("ERROR: User does not exist") #| tell the user and don't do anything
            if (password.verify_password(form.password.data, userObj.hashedPass)): # If the user was found, verify it's password
                if login_user(userObj, remember = rememberBool): # This is how Flask-Login logs in a user
                    return redirect_back('index') # Once the user is logged in, send them to the brevet calculator
            flash("ERROR: Invalid password") # If the password was not correct, then tell the user
        return flask.render_template('login.html', form=form) # Refresh the page so the flashed message can show
    return flask.render_template('login.html', form=form) # This really shouldn't happen

@app.route('/logout')
@login_required
def logout():
    """
    This is a basic flask route for logout, it uses Flask-Login to logout the currently logged-in user
    """
    logout_user()
    return redirect('/login')

##
# From here to the API section are functions that I found online to protect against CSRF and validate nextpage on logins

# These next 2 Functions are from Dan Jacob's CSRF Protection code snippet on flask's site

@app.before_request
def csrf_protect():
    if request.method == "POST":
        if request.values.get('username') != "": # This is weird, but it's neccesary to allow curl requests to register users
            pass
        else:
            token = session.pop('_csrf_token', None)
            if not token or token != request.form.get('_csrf_token'):
                return "CSRF_FAILURE", 401

def generate_csrf_token():
    if '_csrf_token' not in session:
        session['_csrf_token'] = "9eagj34yuja9e"
    return session['_csrf_token']

app.jinja_env.globals['csrf_token'] = generate_csrf_token

# The next 3 functions are from Armin Ronacher's Securely Redirect Back code snippet on flask's site

def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

def get_redirect_target():
    for target in request.values.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target

def redirect_back(endpoint, **values):
    target = request.form['next']
    if not target or not is_safe_url(target):
        target = url_for(endpoint, **values)
    return redirect(target)

###############
# API SECTION #
###############

# API Resources

class listAll(Resource):
    """
    The listAll resource grabs the top x items from the sorted list and returns them in json format,
    where x is the value passed in using the ?top=x query parameter. If no query parameter is specified,
    it will simply return all of the items in the sorted list.
    """
    @needTokenOrLogin
    def get(self):
        topArg = request.args.get('top', '')   #| Get the 'top' query parameter if it exists.
        if (topArg):                           #| If it does exist, then 
            try:                               #| we use this try to check if its an int
                int(topArg)                    #| this fails if the 'top' parameter isnt a number
                getTop = int(topArg)           #| if it was a number, set it as our number of objects to get.
            except ValueError:                 #| if the parameter isn't a number, we go here
                getTop = 20                    #| and just default to getting all 20 entries.
        else: getTop = 20                      #| If the query parameter doesn't exist, just default to getting all 20 entries.
        data = mongo_handler.get_data(getTop) # Then finally get the entries
        if(data):                       #| If we actually got any data,
            return flask.jsonify(data)  #| then return it.
        else:                           #| Otherwise,
            return 'No data'            #| return 'No data'

class listAllArgs(Resource):
    """
    This is a version of the listAll resource that takes another argument like this: listAll/<argument>
    If the argument is 'json' it will return the top x items in json format, and if the argument is 'csv'
    it will return the top x items in csv format, where x is the value passed in using the ?top=x query paramenter.
    If no parameter is specified, it will simply return all of the items in the sorted list.
    """
    @needTokenOrLogin
    def get(self, format_arg):                 #| This is just the same as the first |ed comment as the non format_arg version
        topArg = request.args.get('top', '')   #| I won't be commenting repeated lines here
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        format_arg = format_arg.upper()        # Get the part after listAllArgs/<here> and convert it to uppercase so we can compare w/o capitalization mattering
        data = mongo_handler.get_data(getTop)  # More repeated code
        if(data):
            if(format_arg == "JSON"):       # If the format argument is JSON, then it just returns the object in JSON format as usual
                return flask.jsonify(data)
            elif(format_arg == "CSV"):      # Otherwise, we need to return it in csv format
                csvString = "Distance, Opening, Closing" + '\n'  # The first line is a template for how the data is formatted
                for line in data:                                # Then we loop through each entry in data and add it to the csv string in comma-seperated format
                    if(line["distance"] != ''): csvString = csvString + str(line["distance"]) + ", " + str(line["opening"]) + ", " + str(line["closing"]) + '\n'
                return csvString    # And finally, we return the csv data
            else:
                return "Invalid format"  # Otherwise, we return "Invalid format"
        else:
            return 'No data'

class listOpenOnly(Resource):
    """
    This is a copy of listAll except it removes the 'closing' value before returning data
    """
    @needTokenOrLogin
    def get(self):
        topArg = request.args.get('top', '')
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        data = mongo_handler.get_data(getTop)
        if(data):
            for element in data:
                if 'closing' in element: element.pop('closing', None)  # Loop through the data and remove the "closing" elements
            return flask.jsonify(data)
        else:
            return 'No data'

class listOpenOnlyArgs(Resource):
    """
    This is a copy of listAllArgs except it removes the 'closing' value before returning data
    """
    @needTokenOrLogin
    def get(self, format_arg):
        topArg = request.args.get('top', '')
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        format_arg = format_arg.upper()
        data = mongo_handler.get_data(getTop)
        if(data):
            for element in data:
                if 'closing' in element: element.pop('closing', None)
            if(format_arg == "JSON"):
                return flask.jsonify(data)
            elif(format_arg == "CSV"):
                csvString = "Distance, Opening" + '\n'
                for line in data:
                    if(line["distance"] != ''): csvString = csvString + str(line["distance"]) + ", " + str(line["opening"]) + '\n'
                return csvString
            else:
                return "Invalid format"
        else:
            return 'No data'

class listCloseOnly(Resource):
    """
    This is a copy of listAll except it removes the 'opening' value before returning data
    """
    @needTokenOrLogin
    def get(self):
        topArg = request.args.get('top', '')
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        data = mongo_handler.get_data(getTop)
        if(data):
            for element in data:
                if 'opening' in element: element.pop('opening', None)  # Loop through the data and remove the "opening" elements
            return flask.jsonify(data)
        else:
            return 'No data'

class listCloseOnlyArgs(Resource):
    """
    This is a copy of listAllArgs except it removes the 'opening' value before returning data
    """
    @needTokenOrLogin
    def get(self, format_arg):
        topArg = request.args.get('top', '')
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        format_arg = format_arg.upper()
        data = mongo_handler.get_data(getTop)
        if(data):
            for element in data:
                if 'opening' in element: element.pop('opening', None)
            if(format_arg == "JSON"):
                return flask.jsonify(data)
            elif(format_arg == "CSV"):
                csvString = "Distance, Closing" + '\n'
                for line in data:
                    if(line["distance"] != ''): csvString = csvString + str(line["distance"]) + ", " + str(line["closing"]) + '\n'
                return csvString
            else:
                return "Invalid format"
        else:
            return 'No data'

# API routes

api.add_resource(listAll, '/listAll')
api.add_resource(listAllArgs, '/listAll/<format_arg>')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listOpenOnlyArgs, '/listOpenOnly/<format_arg>')
api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listCloseOnlyArgs, '/listCloseOnly/<format_arg>')

#######################
# Basic Pages SECTION #
#######################


@app.route("/")
@app.route("/index")
@login_required
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    getDate = request.args.get('date')
    getTime = request.args.get('time')
    brevet_dist_total = int(request.args.get('total_dist'))
    timeString = getDate + " " + getTime
    # Looks like "2000-06-10 12:00"
    formattedTime = arrow.get(timeString, 'YYYY-MM-DD HH:mm')
    formattedTime = formattedTime.replace(tzinfo='US/Pacific')
    formattedTime = formattedTime.isoformat()

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, brevet_dist_total, formattedTime)
    close_time = acp_times.close_time(km, brevet_dist_total, formattedTime)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route("/_submit_values")
def _submit_values():
    """
    This function takes the data given to it from calc.html's submit_values() function, which is in the form of
    values read from the html page paired with a distance, open time or close time. It then attempts to iterate 
    through them if and adds each three value row to the mongodb. It then responds that it succeeded in adding them.
    If no data is recieved from calc.html, then it responds that there was an error.
    """
    result = {"printme" : "Error: No data entered."}          # Prepare the base case response first in case nothing happens
    if (request.args.get('control_point_distance_1') != ''):  #| Then if the function got any data at all then we're replacing the 
        mongo_handler.reset()                                 #| current data with it, so delete it all.
    for i in range(20):     # Then iterate through the data
        iterator = i + 1    # Increase the iterator since the data from calc.html is 1-indexed
        tempDist = request.args.get('control_point_distance_' + str(iterator))  # Put each arguement from that # iterator into a temp var
        if(tempDist != ''): tempDist = int(tempDist) ############### added this ################################
        tempOpen = request.args.get('open_time_' + str(iterator))
        tempClose = request.args.get('close_time_' + str(iterator))
        if (tempDist != ''):  # Then if that variable is not empty, that means the row has data in it
            result.update({"printme" : "Saved to database"})     #| And we can add it to the mongodb. Also, change the response
            mongo_handler.add_row(tempDist, tempOpen, tempClose) #| so it says we saved data because we've done that at least once
    return flask.jsonify(result=result)  # And return the response for calc.html to print

@app.route("/_return_values")
def _return_values():
    """
    This function gets the data from the mongodb (if there is any) and returns it to calc.html's display_saved() function
    so that that function can display it. The data comes from mongodb in a list of sets of three key-value pairs for 
    distance, open, and close times. If there is no data saved in the db, then it returns an error for calc.html to
    display.
    """
    data = mongo_handler.get_data(20)     # Take all the data saved in the mongodb and put it into data.
    if (data):  # If there's data in the db
        return flask.jsonify(result=data) # Then return it to the js function in calc.html so that it can display it
    else:       # If there's no data in the db
        result = {"printme" : "Error: No data saved."}  # Return an error to the js in calc.html so that it can display it.
        return flask.jsonify(result=result)
    return "No clue how this happened Lol"  # Catchall case just in case. This shouldnt happen.

@app.route("/_delete_values")
def _delete_values():
    """
    This function tells the mongodb to delete all the data in the collection. It then returns a message saying if it
    succeeded or failed for calc.html to display. This function is not strictly neccessary for the project, but it makes
    testing empty database responses easier, since the mongodb container will save the data between builds.
    """
    data = mongo_handler.get_data(20)
    if (data):
        mongo_handler.reset()
        result = {"printme" : "Saved brevet deleted."}
    else:
        result = {"printme" : "Error: No data to delete."}
    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
