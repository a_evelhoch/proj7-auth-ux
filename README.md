# Project 4: Brevet time calculator with Ajax, RESTful API, token authorization and login

A RUSA ACP controle time calculator made to work with flask and ajax. It also features an API to access the saved brevet data. Additionally, it features a basic authorization system- you must register a user and log them in if you want to use the brevet calcualtor. Also, the API features require either a logged-in user or an authorization token to view.

### Student Details

Name: Andrew Evelhoch
E-mail: aevelhoc@uoregon.edu

## Running the calculator

To run the brevet time calculator, the first step will be to clone the repo to your machine. Then, navigate a terminal to the `./proj7-auth-ux/brevets/` folder, and use the `docker-compose up` command to start the containers up. Once the server is up and running, you can navigate to `localhost:5000` in your browser to access the brevet time calculator. But first, you must be an authorized user. Enter a username and password into the text boxes, then press register to register a new user. Once the user is registered, if you enter the username and password again and press login to log the user in. Now you can access the calculator! To use it, enter in the starting date and time, and total brevet distance (optional). You can then enter in the distance to each controle point in either miles or kilometers, and the page will tell you what time the controle point should open and close. To shut down the server, go into the terminal window and press `Ctrl+C`.

### Saving your brevets

The calculator was enhanced to have the ability to save a brevet time. Once you've entered all your data in the table, press the "Submit brevet" button to save it in the database. You can then press the "Display saved brevet" button to pop up that data in a textbox. There is also a "Delete saved brevet" button, in case you want the database to forget the data you entered. This is not neccessary before submitting another brevet, since that will automatically replace the previous one.

## The RESTful API

Once the calculator is up and running, you can access the API. The API behaves as follows:

* The default paths of the API return all the data:
    * `http://<host:port>/listAll` - returns all open and close times in the database
    * `http://<host:port>/listOpenOnly` - returns open times only
    * `http://<host:port>/listCloseOnly` - returns close times only
* Each API path also can return two different representations: json or csv. To access them, do the following:
    * `http://<host:port>/listAll/csv` - add "/csv" to any of the API paths to make the API return the data in CSV format
    * `http://<host:port>/listOpenOnly/json` - add "/json" to any of the API paths to make the API return the data in JSON format
* Each API path also has a query parameter to get the top "k" open and close times, where k is a number you supply. To access them, do the following:
    * `http://<host:port>/listAll?top=4` - add "?top='k'" after any of the APIs to limit it to the top 'k' brevets only.
    * `http://<host:port>/listCloseOnly/csv?top=8` - you can also add the query parameter after one of the format request paths

You must be logged in to use the APIs, or provide authentication in another way.

## The Authentication API

You can access the authentication functions of the API through a terminal using curl instead of through the login page on the browser, if you wish.

Here are the options:

* Register a user:
`curl -d "username=<name>&password=<pass>" -X POST http://localhost:5000/api/register` - This will register a new user with the username <name> and the password <pass>.
* View a registered user:
`curl -X GET http://localhost:5000/api/user/<id>` - This will return the username of the user with id equal to <id>.
* View brevet database APIs with password authorization:
`curl -u <name>:<pass> -X GET http://localhost:5000/<APIaddress>` - For any of the APIs in the 'RESTful API' section above (for instance, `listAll` or `listOpenOnly/csv?top=3` in place of <APIaddress>), to access them you must use the -u flag and provide a valid usename in <name>'s place and password in <pass>'s place, otherwise you will be denied access.
* Get authorization token
`curl -u <name>:<pass> -X GET http://localhost:5000/api/token` - If you enter a registered user's username and password as <name> and <pass> into the get authorization token function, it will set an authorization token on the server, and let you access the APIs from the previous section without having to supply a username or password. It will also return the authorization token.

### Additional authentication features

The website's login page features csrf protection, nextpage validation and a toggle to remember the user after the web browser is closed.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.

### A brief description of the algorithm

According to this table:
Distance to Controle Point (km) | Minimum Speed (kmph) | Maximum Speed (kmph)
--- | --- | ---
0-200 | 15 | 34
201-400 | 15 | 32
401-600 | 15 | 30
601-1000 | 11.428 | 28
1001-1300 | 13.333 | 26

A controle points opening time is based on the maximum speed in the table, and the closing time is based on the minimum speed. The way it is calculated is that for the distance to the controle point, each portion of the distance of the controle point is divided by the corresponding speed in the same row (minimum speed to determine closing time, maximum speed to determine opening time) and those are all summed up. For instance, an opening time for controle point at a distance of 750 would be 200/34 + 200/32 + 200/30 + 150/28. (24.1561162...). Once those are all summed up, taking the non-decimal portion of the total will give you the number of hours past the starting time that the controle pointe opens. (24 hours) To get the number of minutes, the non-decimal portion is removed, and the fractional portion is multiplied by 60 and rounded to the nearest whole number. (24 hours, 9 minutes)

### The relaxed closing time rule

Because using the simple algorith will cause any controle points less than 60km away from the start will end up with controle points that close before the starting point does, there is a rule to make them stay open longer. For Distances less than 60km, the minimum speed is treated as 20 kmph for the algorithm, and then 1 hour is added afterwards. This only applies if the controle point is less than 60km, you don't need to break the first Distance to controle point into 0-60 and 61-200. This version of the calculator uses logic that might not respect this rule.
